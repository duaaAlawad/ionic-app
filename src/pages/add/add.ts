import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StudentItem } from '../../app/models/students/student-interface';
import { AngularFireDatabase,FirebaseListObservable } from "angularfire2/database-deprecated"

/**
 * Generated class for the AddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add',
  templateUrl: 'add.html',
})
export class AddPage {
  std ={} as StudentItem;
  studentRef$:FirebaseListObservable<StudentItem[]>
  constructor(private database :AngularFireDatabase ,public navCtrl: NavController, public navParams: NavParams) {
    this.studentRef$=this.database.list('student-list');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPage');
  }


  addstudent(std:StudentItem)
  {
 this.studentRef$.push({
  studentname : this.std.studentname,
  studentnumber:Number(this.std.studentnumber)

 });
  }


}
