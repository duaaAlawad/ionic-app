import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { Component, ViewChild } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { AddPage} from '../add/add';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  @ViewChild('username') user;
  @ViewChild('password') password;
  constructor(private alertCtrl: AlertController,private fire : AngularFireAuth,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }



  alert(string)
  {
    this.alertCtrl.create({
      title: 'Info',
      subTitle: 'Message',
      buttons: ['OK']
    }).present();

    
  }
  signin()
  {

    this.fire.auth.signInWithEmailAndPassword(this.user.value, this.password.value).then( data=>{
      console.log('got some data',this.fire.auth.currentUser.displayName);
      this.alert('Success ,  you are log in');
      this.navCtrl.setRoot(AddPage)
    })
    .catch(error =>{
      console.log('got an error',error);

    })
    console.log('Would sign in with ',this.user.value,this.password.value);

  } 





}
