import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
//import {  FirebaseObjectObservable } from 'angularfire2/database';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { AddPage } from '../pages/add/add';

const firebaseConfig={
  apiKey: "AIzaSyCIp9ujehqymXJ8mEJWOXHeJjEj0jM6wus",
  authDomain: "myapp-5e032.firebaseapp.com",
  databaseURL: "https://myapp-5e032.firebaseio.com",
  projectId: "myapp-5e032",
  storageBucket: "myapp-5e032.appspot.com",
  messagingSenderId: "1008531559646"
};


@NgModule({
  declarations: [
    MyApp,
    HomePage,LoginPage,RegisterPage,AddPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFireDatabaseModule,



  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,LoginPage,RegisterPage,AddPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
